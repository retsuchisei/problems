#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <string>
#include <iterator>
#include <stdio.h>
#include <stdlib.h>
#include <set>
#include <unordered_set>
#include <map>
#include <queue>
#include <tuple>
#include <stack>
#include <deque>
#include <climits>
#include <fstream>
#include <cmath>
#include <cstring>
 
typedef long long ll;
typedef long double ld;
 
#define pb push_back
#define mp make_pair
#define f first
#define s second
#define pll pair<ll, ll>
#define pdd pair<ld, ld>
 
using namespace std;
 
 
 
int main()
{
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
 
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);
    //cout << fixed
    //     << setprecision(3);
 
 
    ll n;
    cin >> n;
    n *= 2;
    vector<ll> ra(n);
    for(ll i = 0; i < n; ++i){
        cin >> ra[i];
    }
 
    ll ans = 1e10;
    sort(ra.begin(), ra.end());
 
    for (ll i = 0; i < n; ++i){
        for (ll j = i + 1; j < n; ++j){
            vector<ll> b;
            for (ll k = 0; k < n; ++k){
                if(k == i || k ==j)
                    continue;
                b.pb(ra[k]);
            }
            ll cur = 0;
            for (ll k = 0; k < n-2; k += 2){
                cur += b[k+1] - b[k];
            }
            ans = min(ans, cur);
        }
    }
    cout << ans;
    return 0;
 
}
