#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <string>
#include <string.h>
#include <iterator>
#include <stdio.h>
#include <stdlib.h>
#include <set>
#include <unordered_set>
#include <map>
#include <unordered_map>
#include <queue>
#include <tuple>
#include <stack>
#include <deque>
#include <climits>
#include <fstream>
#include <cmath>
#include <ctime>
//#include <windows.h>

typedef long long ll;
typedef long double ld;



#define pb push_back
//#define mp make_pair
#define pll pair<ll, ll>
#define pdd pair<ld, ld>



using namespace std;

vector<ll> t;

void build (vector<ll> a, ll v, ll tl, ll tr) {
if (tl == tr) {
    t[v] = a[tr];
} else {
    ll tm = (tl + tr) / 2;
    build (a, v * 2, tl, tm);
    build (a, v * 2 + 1, tm + 1, tr);
    t[v] = min(t[v * 2], t[v * 2 + 1]);
}
}

/*ll sum (ll v, ll tl, ll tr, ll l, ll r) {
if (l > r) {
    return 0;
}
if (l == tl && r == tr) {
    return t[v];
}
ll tm = (tl + tr) / 2;

return sum (v * 2, tl, tm, l, min(r, tm)) + sum (v * 2 + 1, tm + 1, tr, max(tm + 1, l), r);
}*/

ll mmin (ll v, ll tl, ll tr, ll l, ll r) {
if (l > r)
    return INT_MAX;
if (l == tl && r == tr) {
    return t[v];
}
ll tm = (tl + tr) / 2;
return min(mmin(v * 2, tl, tm, l, min(tm, r)), mmin(v * 2 + 1, tm + 1, tr, max(l, tm + 1), r));

}

void update (ll v, ll tl, ll tr, ll pos, ll new_val) {
if (tl == tr) {
    t[v] = new_val;
} else {
    ll tm = (tr + tl) / 2;
    if (pos <= tm) {
        update (v * 2, tl, tm, pos, new_val);
    } else {
        update (v * 2 + 1, tm + 1, tr, pos, new_val);
    }
    t[v] = t[v * 2] + t[v * 2 + 1];
}
}

int main()
{

ios::sync_with_stdio(0);
cin.tie(0);
cout.tie(0);
freopen("stupid_rmq.in", "r", stdin);
freopen("stupid_rmq.out", "w", stdout);
//cout << fixed
//     << setprecision(3);
ll n, m;
cin >> n;
t.resize(4 * n);
vector<ll> a(n);

for (ll i = 0; i < n; ++i) {
    cin >> a[i];
}

build(a, 1, 0, n - 1);

cin >> m;

for ( ll i = 0; i < m; ++i) {
    ll l, r;
    cin >> l >> r;
    cout << mmin (1, 0, n - 1, l - 1, r - 1) << '\n';
}




return 0;

}
