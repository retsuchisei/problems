#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <string>
#include <iterator>
#include <stdio.h>
#include <stdlib.h>
#include <set>
#include <unordered_set>
#include <map>
#include <queue>
#include <tuple>
#include <stack>
#include <deque>
#include <climits>
#include <fstream>
#include <cmath>
 
 
#define pb push_back
#define mp make_pair
#define f first
#define s second
 
 
typedef long long ll;
typedef long double ld;
 
using namespace std;
 
ll Nod(ll a, ll b)
{
    while (a && b)
        if (a >= b)
           a %= b;
        else
           b %= a;
    return a | b;
}
 
int main()
{
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
 
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);
    cout << fixed
         << setprecision(9);
 
    ll n;
    cin >> n;
    ll a, b;
    for(ll i = 0; i <= n / 2; ++i){
        if(Nod(i, n - i) == 1){
            a = i;
            b = n - i;
        }
    }
    cout << a << ' ' << b;

    return 0;
}
