#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <string>
#include <iterator>
#include <stdio.h>
#include <stdlib.h>
#include <set>
#include <unordered_set>
#include <map>
#include <queue>
#include <tuple>
#include <stack>
#include <deque>
#include <climits>
#include <fstream>
#include <cmath>
#include <cstring>

typedef long long ll;
typedef long double ld;

#define pb push_back
#define mp make_pair
#define mt make_tuple
#define f first
#define s second
#define pll pair<ll, ll>
#define pdd pair<ld, ld>

using namespace std;

const ll INF = 1e10;


int main()
{
ios::sync_with_stdio(0);
cin.tie(0);
cout.tie(0);

//freopen("in.txt", "r", stdin);
//freopen("out.txt", "w", stdout);
//cout << fixed
 //    << setprecision(9);


ll n;
cin >> n;
ll ans[101] = {0};
for (ll i = 0; i < n; ++i){
    ll c;
    cin >> c;
    ans[c]++;
}
for (ll i = 1; i < 101; ++i){
    if(ans[i] == n/2){
        for (ll j = i+1; j < 101; ++j){
            if(ans[i] == ans[j])
                return cout << "YES\n" << i << ' ' << j, 0;
        }
    }
}
cout << "NO";
return 0;

}
