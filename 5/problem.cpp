#include <iostream>
#include <iomanip>
#include <vector>
#include <set>
#include <map>
#include <algorithm>
#include <iterator>
#include <cmath>
#include <stdlib.h>
#include <climits>
 
 
#define pb push_back
#define mp make_pair
#define f first
#define s second
 
 
typedef long long ll;
typedef long double ld;
 
using namespace std;
 
const ll max = 3*10e5;
 
 
 
 
int main()
{
    ios::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
 
    //freopen("out.txt", "w", stdout);
    //freopen("in.txt", "r", stdin);
 
    ll k, count = 0;
    string str = "";
    cin >> k;
 
    if(k==0)
        return cout << "ab", 0;
    while(k != 0){
 
        ll l = 0, r = 100000;
        while(r - l > 1){
            ll m = (r+l)/2;
 
            if((m*m + m)/2 <= k)
                l = m;
            else
                r = m;
        }
 
        k -= (l*l + l) / 2;
 
        for(ll i = 0; i <= l; ++i)
            str += (char)('a' + count);
        ++count;
    }
 
    cout << str;
 
    return 0;
}
