#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <string>
#include <iterator>
#include <stdio.h>
#include <stdlib.h>
#include <set>
#include <unordered_set>
#include <map>
#include <queue>
#include <tuple>
#include <stack>
#include <deque>
#include <climits>
#include <fstream>
 
 
using namespace std;
typedef long long ll;
typedef long double ld;
int INF = INT_MAX;
const int maxn = 500000;
vector<int> g[maxn];
int  mas[2*maxn][20], order[2*maxn], h[2*maxn], first[maxn] = {0};
int timer = 1;
 
int rmq(int i, int j)
{
	if (i > j) swap(i,j); 
	int k = 0;
	while ((1 << (k + 1)) <= j - i + 1) k++;
	int res = mas[i][k];
	if (h[mas[j - (1<<k) + 1][k]] < h[res])
	  res = mas[j - (1<<k) + 1][k];
	return res;
}
 
void dfs(int v, int p = 0, int h1 = 0)
{
	h[timer] = h1;
	order[timer] = v;
	first[v] = timer++;
	for(int i = 0; i < g[v].size(); i++)
	{
		int to = g[v][i];
		if (to != p)
		{
			dfs(to, v, h1 + 1);
			order[timer] = v;
			h[timer++] = h1;
		}
	}
}
 
int lca(int i, int j)
{
  int index = rmq(first[i],first[j]);
  return order[index];
}
 
 
int main(){
	ios::sync_with_stdio(0); 
	cin.tie(0); 
	cout.tie(0);
	freopen("lca.in","r",stdin);
	freopen("lca.out","w",stdout);
 
	int n;
	cin >> n;
	vector<pair<int, int> > get;
 
	for (int i = 0; i < n; ++i)
	{
		int a, b;
		string s;
		cin >> s >> a >> b;
 
		if(s == "ADD")
		{	
			g[a].push_back(b);
			g[b].push_back(a);
		}	
		else 		
			get.push_back(make_pair(a, b));
	}
 
	dfs(timer);
	int i, j;
	for (i = 0; i <= timer; i++) mas[i][0] = i;
	for (j = 1; 1 << j < timer; j++)
		for (i = 1; i + (1 << j) - 1 < timer; i++)
			if (h[mas[i][j - 1]] < h[mas[i + (1 << (j - 1))][j - 1]])
				 mas[i][j] = mas[i][j - 1];
			else
				mas[i][j] = mas[i + (1 << (j - 1))][j - 1];
	  
	for ( auto it : get)
		cout << lca(it.first, it.second) << endl;
 
 
	return 0;
}
